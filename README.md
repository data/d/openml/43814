# OpenML dataset: Dubai-Properties---Apartments

https://www.openml.org/d/43814

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

This dataset is part of my personal project Apartment Pricing: Advance Regression Techniques. You can check the details on the below URL:
Apartment Pricing: Advanced Regression Techniques
The data is scraped from the real estate portal and it is anonymized. It consists of more than 1800+ properties containing 38 features.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43814) of an [OpenML dataset](https://www.openml.org/d/43814). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43814/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43814/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43814/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

